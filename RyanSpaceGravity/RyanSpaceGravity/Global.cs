﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace RyanSpaceGravity
{
    public static class Global
    {
        public static float spawnMass = 10f;
        public static float spawnDensity = 10f;
        public static float gravity = 0.05f; //6.67408f * (float)Math.Pow(10,-11);
        public static float gameScale = 0.012f; //plz don't change senpai
        public static Random gen = new Random();

        public static Vector2 randVector(int low, int high)
        {
            return new Vector2(gen.Next(low, high + 1) + ((float)gen.NextDouble() * gen.Next(-1, 2)), gen.Next(low, high + 1) + ((float)gen.NextDouble() * gen.Next(-1, 2)));
        }
    }
}
