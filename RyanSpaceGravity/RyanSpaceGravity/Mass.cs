﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RyanSpaceGravity
{
    public class Mass
    {
        Texture2D image;
        Vector2 position;
        Color tint;
        

        public float mass;

        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }
        public float X
        {
            get
            {
                return position.X;
            }
            set
            {
                position.X = value;
            }
        }
        public float Y
        {
            get
            {
                return position.Y;
            }
            set
            {
                position.Y = value;
            }
        }

        public Vector2 Velocity { get; set; }
        public Vector2 Acceleration { get; set; }
        public Vector2 Momentum { get; set; }
        public float Density { get; set; }
        public float Volume { get; set; }

        public float Rotation {get; set;}
        public Vector2 Origin { get; set; }
        public float Scale { get; set; }
        public SpriteEffects SpriteEffect { get; set; }
        public Rectangle Hitbox { get; set; }

        
        public bool Outside { get; set; }
        

        public float Radius
        {
            get
            {
                return Hitbox.Width / 2;
            }
        }

        public Mass(Texture2D imgin, Vector2 posin, Color tintin, float massin, float densityin)
        {
            image = imgin;
            position = posin;
            tint = tintin;
            Outside = false;
            mass = massin;
            Density = densityin;

            Scale = Global.gameScale;

            Rotation = 0;
            Volume = massin / Density;
            SpriteEffect = SpriteEffects.None;

            Velocity = Vector2.Zero;
            Acceleration = Vector2.Zero;

            Origin = new Vector2( (image.Width ) / 2, (image.Height) /2);
            Momentum = mass * Velocity;

            
        }

        public void Update(Viewport screen)
        {
            Momentum = mass * Velocity;
            Volume = mass / Density;
            Velocity += Acceleration;
            position += Velocity;
            Hitbox = new Rectangle((int)(position.X - (image.Width * (Volume * Scale)) / 2), (int)(position.Y - (image.Height * (Volume * Scale)) / 2), (int)(image.Width * (Volume * Scale)), (int)(image.Height * (Volume * Scale)));


            if (position.X + Hitbox.Width/2 < 0)
            {
                //Outside = true;
            }
            else Outside = false;

            colorize();
        }

        private void colorize()
        {
            //density high = white
            //blue
            //teal
            //green
            //yellow
            //orange
            //density low = red
            //r g b

            float r = MathHelper.Clamp(50/Density, 0,255);
            float g = MathHelper.Clamp(Density/25,0,255);
            float b = MathHelper.Clamp(Density/50, 0, 255);
          
            tint = new Color(r,g,b);

        }



  




        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(image, position, null, tint, Rotation, Origin, Volume * Scale, SpriteEffect, 1f);
            //spriteBatch.Draw(image, Hitbox, Color.Red);
        }
    }
}
